from setuptools import setup

setup(name="sge_commands",
      version="1.0",
      description="sge_commands",
      author="Daniel Oberti",
      author_email="doberti@marvell.com",
      url="https://imalexissaez.github.io/",
      packages=["sge_commands"])
